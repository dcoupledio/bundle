<?php namespace Decoupled\Core\Bundle;

interface BundleInitializerInterface{

    public function uses( BundleProcessInterface $process );

    public function addProcess( BundleProcessInterface $process );

    public function getProcess( $name );

    public function removeProcess( $name );

    public function init( BundleCollectionInterface $bundle );

}