<?php namespace Decoupled\Core\Bundle;

use Decoupled\Core\Application\ApplicationContainer;

abstract class BundleProcess implements BundleProcessInterface{

    protected $app;

    public function setApp( ApplicationContainer $app )
    {
        $this->app = $app;

        return $this;
    }

    public function getApp()
    {
        return $this->app;
    }

    /**
     * Gets the callback from file, and converts into action
     *
     * @param      string  $file   The file
     *
     * @return     Decoupled\Core\Action\ActionInterface  The action from file.
     */

    public function getActionFromFile( $file )
    {
        if( !file_exists($file) ) return;

        $callback = require($file);

        $app = $this->getApp();

        return $app['$action.factory']->make( $callback );  
    }    

    public abstract function getName();

    public function __invoke( BundleInterface $bundle )
    {
        return $this->process($bundle);
    }

    public abstract function process( BundleInterface $bundle );

}