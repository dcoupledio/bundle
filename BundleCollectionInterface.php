<?php namespace Decoupled\Core\Bundle;

interface BundleCollectionInterface{

    public function get( $id );

    public function add( BundleInterface $bundle );

    public function remove( $id );

    public function all();

}