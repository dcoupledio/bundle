<?php namespace Decoupled\Core\Bundle\Test\Process;

use Decoupled\Core\Bundle\BundleProcessInterface;
use Decoupled\Core\Bundle\BundleInterface;

class RoutingProcess implements BundleProcessInterface{

    public $router;

    public function getName()
    {
        return 'bundle.routing';
    }

    public function __invoke( BundleInterface $bundle )
    {
        return $this->process( $bundle );
    }

    public function process( BundleInterface $bundle )
    {
        $dir = $bundle->getDir();

        $routesFile = $dir.DIRECTORY_SEPARATOR.'routes.php';

        if( file_exists( $routesFile ) )
        {
            $routes = require( $routesFile );

            $routes( $this->router ); 
        }
    }

}