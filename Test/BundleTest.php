<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Bundle\BundleCollection;
use Decoupled\Core\Bundle\Test\Bundle\AppBundle;
use Decoupled\Core\Bundle\Test\Process\RoutingProcess;
use Decoupled\Core\Bundle\BundleInitializer;

class BundleTest extends TestCase{

    public function testCanRunProcess()
    {
        $init = new BundleInitializer();
        $bundles = new BundleCollection();

        $bundles->add( 
            new AppBundle(),
            new AppBundle()
        );

        $process = new RoutingProcess();

        $process->router = (object) [];

        $init->uses( $process );

        $init->init( $bundles );

        $this->assertTrue( $process->router->example );
    }

}