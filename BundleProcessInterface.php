<?php namespace Decoupled\Core\Bundle;

interface BundleProcessInterface{

    public function getName();

    public function __invoke( BundleInterface $bundle );

    public function process( BundleInterface $bundle );

}