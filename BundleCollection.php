<?php namespace Decoupled\Core\Bundle;

class BundleCollection implements BundleCollectionInterface{

    protected $bundles = [];

    /**
     * gets the bundle
     *
     * @param      string  $id     The identifier
     *
     * @return     Decoupled\Core\Wordpress\Bundle\BundleInterface  ( the bundle )
     */

    public function get( $id )
    {
        return $this->bundles[$id];
    }

    /**
     * add bundle to collection
     *
     * @param      Decoupled\Core\Wordpress\Bundle\BundleInterface  $bundle  The bundle
     *
     * @return     Decoupled\Core\Wordpress\Bundle\BundleCollection ( self )
     */

    public function add( BundleInterface $bundle )
    {
        $bundles = func_get_args();

        if(count($bundles) > 1)
        {
            foreach($bundles as $bundle)
            {
                $this->add( $bundle );
            }
        }
        else
        {
            $this->bundles[$bundle->getName()] = $bundle;
        }

        return $this;
    }

    /**
     * remove bundle from collection by id
     *
     * @param      string  $id     The identifier
     *
     * @return     Decoupled\Core\Wordpress\Bundle\BundleCollection ( self )
     */

    public function remove( $id )
    {
        unset( $this->bundles[$id] );

        return $this;
    }

    /**
     * get's all bundles
     *
     * @return     array  ( the bundles )
     */

    public function all()
    {
        return $this->bundles;
    }

}