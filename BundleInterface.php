<?php namespace Decoupled\Core\Bundle;

interface BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName();

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir();

}