<?php namespace Decoupled\Core\Bundle;

class BundleInitializer implements BundleInitializerInterface{

    protected $processes = [];

    /**
     * alias for addProcess method
     *
     * @param      Decoupled\Core\Bundle\BundleProcessInterface  $process  The process
     *
     * @return     Decoupled\Core\Bundle\BundleInitializer  ( self )
     */

    public function uses( BundleProcessInterface $process )
    {
        $processes = func_get_args();

        foreach($processes as $process)
        {
            $this->addProcess( $process );
        }

        return $this;
    }

    /**
     * pipes process into bundle initialization
     *
     * @param      Decoupled\Core\Bundle\BundleProcessInterface  $process  The process
     *
     * @return     Decoupled\Core\Bundle\BundleInitializer  ( self )
     */

    public function addProcess( BundleProcessInterface $process )
    {
        $this->processes[$process->getName()] = $process;

        return $this;
    }

    /**
     * Gets the process.
     *
     * @param      string  $name   The name
     *
     * @return     Decoupled\Core\Bundle\BundleProcessInterface The process.
     */

    public function getProcess( $name )
    {
        return $this->processes[$name];
    }

    /**
     * Removes a process.
     *
     * @param      string  $name   The name
     * 
     * @return     Decoupled\Core\Bundle\BundleInitializer  ( self )
     */

    public function removeProcess( $name )
    {
        unset( $this->processes[$name] );

        return $this;
    }


    /**
     * runs each bundle through each processes
     *
     * @param      Decoupled\Core\Bundle\BundleCollectionInterface  $bundle  The bundle
     * 
     * @return     void
     */

    public function init( BundleCollectionInterface $bundle )
    {
        foreach($bundle->all() as $bundle )
        {
            foreach( $this->processes as $process )
            {
                $process( $bundle );
            }
        }
    }

}